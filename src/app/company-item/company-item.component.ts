import {Component, Input, OnInit} from '@angular/core';
import {CompanyModel} from '../model/company.model';
import {ImageService} from '../service/image.service';
import {DataService} from '../service/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-company-item',
  templateUrl: './company-item.component.html',
  styleUrls: ['./company-item.component.css']
})
export class CompanyItemComponent implements OnInit {

  @Input() company: CompanyModel;
  @Input() index: any;

  constructor(public imageService:ImageService,public dataService:DataService,
              private router:Router) {
  }

  ngOnInit() {
  }

  deleteCompany(company: CompanyModel) {
    this.dataService.deleteCompany(company);
  }

  navigateToCompanyDetails(){
    this.router.navigate(['/company-details/'+this.index]);
  }
}
