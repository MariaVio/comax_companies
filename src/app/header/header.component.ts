import {Component, OnInit} from '@angular/core';
import {Direction, LocalizationService} from '../service/localization.service';
import {CdkVirtualScrollViewport, ScrollDispatcher} from '@angular/cdk/scrolling';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{




  constructor(private localizationService:LocalizationService){

  }

  ngOnInit() {
  }

  changeLang(lang: string) {
    this.localizationService.setLanguage(lang);
  }


}
