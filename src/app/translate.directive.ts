import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';
import {LocalizationService} from './service/localization.service';

@Directive({
  selector: '[appTranslate]'
})
export class TranslateDirective implements OnInit{

  // @Input('appTranslate') appTranslate: string;
  str: string;


  constructor(private elRef: ElementRef, private localizationService: LocalizationService) {
    this.localizationService.changed.asObservable().subscribe( changes => {
      this.elRef.nativeElement.innerText = this.translate(this.str);
    })
  }

  ngOnInit(): void {
    this.str = this.elRef.nativeElement.innerText;
    this.elRef.nativeElement.innerText = this.translate(this.str);
  }


  translate(text: string) {
    let propertiesArray = text.split('.');
    let dictionary = this.localizationService.currentDictionary;

    for (let i=0; i < propertiesArray.length; i++) {
      dictionary = dictionary[propertiesArray[i]];
    }
    return dictionary;
  }
}
