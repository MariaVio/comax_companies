import {CompanyModel} from '../model/company.model';
import companiesJson from '../../assets/companies.json';
import {ApplicationRef, Injectable} from '@angular/core';

@Injectable()
export class DataService {

  companyList: CompanyModel[];

  constructor(private appRef: ApplicationRef) {
    this.companyList = companiesJson['companies'];
  }

  addCompany(company: CompanyModel): void {
    this.companyList.push(company);
    this.companyList=[...this.companyList];
    this.appRef.tick();

  }

  deleteCompany(company: CompanyModel): void {
    let index = this.companyList.indexOf(company);
    this.companyList.splice(index, 1);
    this.companyList = [...this.companyList];
    this.appRef.tick();
  }
}
