import enLangJson from '../../assets/lang/en.json';
import heLangJson from '../../assets/lang/he.json';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';


export enum Direction {
  LTR = 'ltr',
  RTL = 'rtl'
}

@Injectable()
export class LocalizationService {

  readonly LANGUAGE = 'LANGUAGE';
  readonly DIRECTION = 'DIRECTION';
  private dictionaries = {
    'en': enLangJson,
    'he': heLangJson
  };
  private directions = {
    'en': Direction.LTR,
    'he': Direction.RTL
  };

  currentLanguage: string;
  currentDirection: string;
  currentDictionary: string;

  changed: Subject<any>;

  constructor()  {
    let lang = localStorage.getItem(this.LANGUAGE);
    this.currentLanguage = lang ? lang : 'en';

    let direction = localStorage.getItem(this.DIRECTION);
    this.currentDirection = direction ? direction : Direction.LTR;

    this.currentDictionary = this.dictionaries[this.currentLanguage];
    this.changed = new Subject<any>();
  }

  setLanguage(language: string): void {
    this.currentLanguage = language;
    localStorage.setItem(this.LANGUAGE, language);
    this.currentDictionary = this.dictionaries[this.currentLanguage];
    this.setDirection(this.directions[this.currentLanguage]);
    this.changed.next();
  }

  setDirection(direction: Direction) {
    this.currentDirection = direction;
    localStorage.setItem(this.DIRECTION, direction);
  }

}
