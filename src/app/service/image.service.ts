import {Injectable, OnInit} from '@angular/core';
import {ImageLink} from '../model/ImageLink.model';

@Injectable()
export class ImageService implements OnInit {
  readonly BASE_IMAGE_PATH = '../../assets/img/';
  imageList: ImageLink[];

  constructor() {
    this.imageList = [];
    let imageLink = new ImageLink();
    imageLink.name = 'ןןיקס';
    imageLink.src = 'https://www.izodiaque.com/wp-content/uploads/2017/09/company-name.png';

    this.addImage('וויקס', this.BASE_IMAGE_PATH+'pic1.jpg');
    this.addImage('קומקס', this.BASE_IMAGE_PATH+'pic2.png');
    this.addImage('תנובה', this.BASE_IMAGE_PATH+'pic3.jpg');
    this.addImage('מייקרוסופט', this.BASE_IMAGE_PATH+'pic4.jpg');
    this.addImage('איי בי אם', this.BASE_IMAGE_PATH+'pic5.jpg');
    this.addImage('גוגל ישראל', this.BASE_IMAGE_PATH+'pic6.jpg');
    this.addImage('סימילר ווב', this.BASE_IMAGE_PATH+'pic7.jpg');
    this.addImage('פידווייזר', this.BASE_IMAGE_PATH+'pic8.jpg');
    this.addImage('בנק הפועלים', this.BASE_IMAGE_PATH+'pic1.jpg');
    this.addImage('פוקס ויזל', this.BASE_IMAGE_PATH+'pic5.jpg');

  }

  ngOnInit(): void {

  }

  addImage(name: string, src: string): void {
    let imageLink = new ImageLink();
    imageLink.name = name;
    imageLink.src = src;
    this.imageList.push(imageLink);
    // this.imageList = [...this.imageList];
  }

  getImage(name: string): string {
    let result = '';
    let images = this.imageList.filter((obj) => {
      return obj.name === name;
    });
    if (images.length != 0) {
      result = (<ImageLink>images[0]).src;
    }

    return result;
  }


}
