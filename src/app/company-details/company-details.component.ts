import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {DataService} from '../service/data.service';
import {CompanyModel} from '../model/company.model';
import {ImageService} from '../service/image.service';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.css']
})
export class CompanyDetailsComponent implements OnInit {
  index: any;
  company: CompanyModel;
  imgSrc: string;
  latitude: any;
  longitude: any;

  constructor(private router: ActivatedRoute, public dataService: DataService,
              public imageService: ImageService) {
  }

  ngOnInit() {
    this.index = this.router.snapshot.params['id'];
    this.router.params.subscribe((params: Params) => {
      this.index = params['id'];
      this.company = this.dataService.companyList[this.index];

    });
    this.company = this.dataService.companyList[this.index];
    this.imgSrc = this.imageService.getImage(this.company.name);
    this.latitude = +this.company.latitude;
    this.longitude =  +this.company.longitude;
  }

}
