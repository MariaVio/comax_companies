import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AddCompanyComponent} from './add-company/add-company.component';
import {CompanyDetailsComponent} from './company-details/company-details.component';

const routes:Routes=[
  {path:'',component:HomeComponent},
  {path:'add-company',component:AddCompanyComponent},
  {path:'company-details/:id',component:CompanyDetailsComponent}
]
@NgModule({
  imports:[RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule {

}
