export class ImageLink {
  name: string;
  src: string;
}
