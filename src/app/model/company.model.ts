export class CompanyModel {
  city:string;
  name:string;
  address:string;
  picture:string;
  latitude:string;
  longitude:string;
}
