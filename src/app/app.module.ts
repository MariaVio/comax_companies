import { BrowserModule } from '@angular/platform-browser';
import {ChangeDetectorRef, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { AddCompanyComponent } from './add-company/add-company.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule} from '@angular/common/http';
import {TranslatePipe} from './pipe/translate';
import {LocalizationService} from './service/localization.service';
import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
import {AgmCoreModule} from '@agm/core';
import {ImageService} from './service/image.service';
import {DataService} from './service/data.service';
import { CompanyItemComponent } from './company-item/company-item.component';
import {HeaderComponent} from './header/header.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {AppRoutingModule} from './app-routing.module';
import { TranslateDirective } from './translate.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    AddCompanyComponent,
    CompanyDetailsComponent,
    TranslatePipe,
    CompanyItemComponent,
    TranslateDirective
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    ScrollingModule,
    MatGoogleMapsAutocompleteModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCEMC-5-RG1snVZdKv3Yke3bxBXAXawwLM',
      // clientId: '<mandatory>',
      // apiVersion: 'xxx', // optional
      // channel: 'yyy', // optional
      // apiKey: 'zzz', // optional
      language: 'en',
      libraries: ['geometry', 'places'],

    }),

  ],
  providers: [LocalizationService,ImageService,DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
