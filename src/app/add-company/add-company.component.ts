import {Component, OnInit} from '@angular/core';
import {Direction, LocalizationService} from '../service/localization.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CompanyModel} from '../model/company.model';
import {ImageService} from '../service/image.service';
import {DataService} from '../service/data.service';
import {Router} from '@angular/router';
import PlaceResult = google.maps.places.PlaceResult;

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css']
})
export class AddCompanyComponent implements OnInit {

  formCompany: FormGroup;
  imgSrc: any;
  place_id: string;
  latitude: string;
  longitude: string;
  currentAddress: string;

  constructor(public localizationService: LocalizationService, private builder: FormBuilder,
              public imageService: ImageService, private dataService: DataService, private router:Router) {
  }

  ngOnInit() {
    this.formCompany = this.builder.group({
        'name': new FormControl(null, Validators.required),
        'city': new FormControl(null, Validators.required),
        'address': new FormControl(null, Validators.required),
        'picture': new FormControl(null, Validators.required),
      },
      {
        validators: [this.checkAddress()]

      });
  }


  //Autocomplete
  checkAddress() {
    return (group: FormGroup) => {
      let addressFromInput = group.controls['address'];
      if (!this.place_id) {
        return addressFromInput.setErrors({notEquivalent: true});
      }
      if (!addressFromInput.value && this.place_id != '') {
        this.place_id = '';
        return addressFromInput.setErrors({notEquivalent: true});
      }
      if (this.place_id) {
        addressFromInput.setErrors({notEquivalent: false});
        return addressFromInput.setErrors(null);
      }
      else {
        return addressFromInput.setErrors(null);
      }

    };
  }

  onAutocompleteSelected(result: PlaceResult) {
    this.place_id = result['place_id'];
    let addressFromInput = this.formCompany.controls['address'];
    addressFromInput.setErrors({notEquivalent: false});
    addressFromInput.setErrors(null);
    this.currentAddress = result.formatted_address;
  }

  onLocationSelected(location: Location) {
    this.latitude = location['latitude'];
    this.longitude = location['longitude'];
  }

  onSubmitForm() {
    console.log(this.formCompany);
    let company = this.createCompany();

    this.imageService.addImage(company.name, this.imgSrc);
    this.dataService.addCompany(company);
    console.log(this.dataService.companyList);
    this.imgSrc = '';
    this.router.navigate(['/']);
  }

  private createCompany() {
    let company = new CompanyModel();
    company.name = this.formCompany.value.name;
    company.city = this.formCompany.value.city;
    company.latitude = this.latitude;
    company.longitude = this.longitude;
    company.address = this.currentAddress;
    return company;
  }

  onResetForm() {
    this.formCompany.reset();
  }

  onFileSelected(files: FileList | null) {
    this.formCompany.get('picture').setValue(files[0].name);
    let reader = new FileReader();
    reader.onload = ((event: Event) => {
      this.imgSrc = reader.result;
    });
    if (files[0]) {
      reader.readAsDataURL(files[0]);
    }

  }


}
