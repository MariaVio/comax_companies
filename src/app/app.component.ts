import {Component, EventEmitter, Output} from '@angular/core';
import {LocalizationService} from './service/localization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  direction = 'rtl';

  constructor(public localizationService: LocalizationService) {
  }
}
