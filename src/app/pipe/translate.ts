import {Pipe, PipeTransform} from '@angular/core';
import { LocalizationService} from '../service/localization.service';

@Pipe({
  name: 'translate',
  pure: true
})

export class TranslatePipe implements PipeTransform {


  constructor(private localizationService: LocalizationService) {
  }

  transform(varName: string, lang: string): string {
    let propertiesArray = varName.split('.');
    let dictionary = this.localizationService.currentDictionary;

    for (let i=0; i < propertiesArray.length; i++) {
      dictionary = dictionary[propertiesArray[i]];
    }
    return dictionary;
  }
}
