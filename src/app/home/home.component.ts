import {Component, OnInit} from '@angular/core';
import {DataService} from '../service/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  hidden = false;

  constructor(public dataService: DataService, private router: Router) {
  }

  ngOnInit() {
   }

  addCompany() {
    this.router.navigate(['/add-company']);
  }

  deleteAll() {
    this.dataService.companyList = [];
    this.dataService.companyList = [...this.dataService.companyList];
  }
}
